/// ### Global variables
// global timer, used to update the display
var timer;

// the date when the pomodor session starts, used to accurately compute the remaining time
var startDate;

// the current state of the pomodoro (stopped, working, shortBreak, longBreak)
var currentState = "stopped";

// ### Settings
// duration of a work step
var work_duration;
// duration of a short break
var short_duration;
// work step quantity
var work_qty;
// duration of a long break
var long_duration;
// the current timer duration
var currentDuration;
// the current work iteration
var currentWorkIteration;

// ### Sounds
// beep low
var beep_low;
// beep high
var beep_high;
// work sound
var workSound;
// break sound
var breakSound;

// ### Display handling
// minute HTML field
var minuteField;
// second HTML field
var secondField;
// current step label
var currentStepLabel;

// ### Controls
// Start/Stop button
var startStopButton;
// Setup button
var setupButton;

// initialize the application
function init() {
    // init audio sounds
    beep_low = new Audio("sound/beep_low.ogg");
    beep_high = new Audio("sound/beep_high.ogg");
    workSound = new Audio("sound/start-work.mp3");
    breakSound = new Audio("sound/start-break.mp3");
    // get values from fields
    setValues();
    
    // init config modal window
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the button that opens the modal
    setupButton = document.getElementById("setupButton");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal 
    setupButton.onclick = function() {
        modal.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    setupButton.click();
}

// configure the current session using fields.
function setup() {
    setValues();
}

// changes the state of the app
function changeState(state) {
    currentState = state;
    startDate = new Date();
    switch (state) {
        case "working":
            // setup values
            currentDuration = work_duration;
            currentWorkIteration = 1;
            // setup display
            currentStepLabel.innerHTML = "Working...";
            startStopButton.innerHTML = "Stop Pomodoro";
            startStopButton.onclick = function () {
                changeState('stopped')
            };
            startStopButton.classList = "btn btn-stop";
            setupButton.style.visibility = 'hidden';
            updateDisplay(state);
            // play sound
            workSound.play();
            // start the timer for display refreshing
            timer = window.setInterval(updateTimer, 100);
            break;
        case "shortBreak":
            // setup values
            currentDuration = short_duration;
            // play sound
            breakSound.play();
            // setup display
            currentStepLabel.innerHTML = "Short break";
            startStopButton.classList = "btn btn-short";
            // start the timer for display refreshing
            timer = window.setInterval(updateTimer, 100);
            break;
        case "longBreak":
            // setup values
            currentDuration = long_duration;
            // play sound
            breakSound.play();
            // setup display
            currentStepLabel.innerHTML = "Long break";
            startStopButton.classList = "btn btn-long";
            // start the timer for display refreshing
            timer = window.setInterval(updateTimer, 100);
            break;
        case "stopped":
            clearInterval(timer);
            currentStepLabel.innerHTML = "Stopped";
            startStopButton.innerHTML = "Start Pomodoro";
            startStopButton.onclick = function () {
                setup();
                changeState('working')
            };
            startStopButton.classList = "btn btn-start";
            setupButton.style.visibility = 'visible';
            break;
    }
    updateDisplay(state);
}

// update the display (colors and step label)
function updateDisplay(state) {
    document.querySelector("body").classList = state;
}

// update the timer display
function updateTimer() {
    var diff = getTimeDifference(startDate, new Date());

    // while the work_duration is not finished
    if (currentDuration * 60 - diff.totSeconds > 0) {
        if (diff.millis % 1000 < 100) {
            var minutes = currentDuration - 1 - diff.minutes;
            var seconds = 59 - diff.seconds;
            if (seconds <= 5 && minutes == 0) {
                if (seconds % 2 == 0) {
                    beep_high.play();
                } else {
                    beep_low.play();
                }
            }
            minutes = String("00" + minutes).slice(-2);
            seconds = String("00" + seconds).slice(-2);
            minuteField.innerHTML = minutes;
            secondField.innerHTML = seconds;
            // update the title bar
            document.title = "[" + currentState.slice(0, 1).toUpperCase() + "] " + minutes + ":" + seconds;
        }
    } else {
        // time to switch to as work or break step
        // first we stop the timer
        clearInterval(timer);
        nextState();
    }
}

// compute the nextState according to the current one
function nextState() {
    if (currentState == "working" && currentWorkIteration != work_qty) {
        work_qty--;
        changeState("shortBreak");
    } else if (currentState == "working" && currentWorkIteration == work_qty) {
        work_qty = document.getElementById("work_qty").value;
        changeState("longBreak");
    } else if (currentState == "shortBreak" || currentState == "longBreak") {
        changeState("working");
    }
}

// set field values
function setValues() {
    // init variables
    work_field = document.getElementById("work_duration");
    work_duration = work_field.value;
    short_duration = document.getElementById("short_duration").value;
    work_qty = document.getElementById("work_qty").value;
    long_duration = document.getElementById("long_duration").value
    minuteField = document.getElementById("minutes");
    secondField = document.getElementById("seconds");
    startStopButton = document.getElementById("startStopButton");
    currentStepLabel = document.getElementById("currentStepLabel");
    currentStepLabel.innerHTML = "Stopped";
    // nicely display on 2 digits
    minuteField.innerHTML = String("00" + work_duration).slice(-2);
    secondField.innerHTML = "00";
}

// compute the time difference between two dates
function getTimeDifference(earlierDate, laterDate) {
    var nTotalDiff = laterDate.getTime() - earlierDate.getTime();
    var oDiff = new Object();

    oDiff.millis = nTotalDiff;

    oDiff.totSeconds = Math.floor(nTotalDiff / 1000);

    oDiff.days = Math.floor(nTotalDiff / 1000 / 60 / 60 / 24);
    nTotalDiff -= oDiff.days * 1000 * 60 * 60 * 24;

    oDiff.hours = Math.floor(nTotalDiff / 1000 / 60 / 60);
    nTotalDiff -= oDiff.hours * 1000 * 60 * 60;

    oDiff.minutes = Math.floor(nTotalDiff / 1000 / 60);
    nTotalDiff -= oDiff.minutes * 1000 * 60;

    oDiff.seconds = Math.floor(nTotalDiff / 1000);

    return oDiff;
}
